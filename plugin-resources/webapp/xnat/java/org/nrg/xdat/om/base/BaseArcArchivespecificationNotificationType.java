/*
 * GENERATED FILE
 * Created on Wed Apr 25 16:18:39 CDT 2012
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseArcArchivespecificationNotificationType extends AutoArcArchivespecificationNotificationType {

	public BaseArcArchivespecificationNotificationType(ItemI item)
	{
		super(item);
	}

	public BaseArcArchivespecificationNotificationType(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseArcArchivespecificationNotificationType(UserI user)
	 **/
	public BaseArcArchivespecificationNotificationType()
	{}

	public BaseArcArchivespecificationNotificationType(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
