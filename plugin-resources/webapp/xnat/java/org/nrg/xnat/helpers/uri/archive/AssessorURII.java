package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatImageassessordata;


public interface AssessorURII {
	public XnatImageassessordata getAssessor();
}
