package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatExperimentdata;

public interface ExperimentURII {
	public XnatExperimentdata getExperiment();
}
