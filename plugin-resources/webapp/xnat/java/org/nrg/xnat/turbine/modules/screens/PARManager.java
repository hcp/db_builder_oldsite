/**
 * Copyright 2008 Washington University - All rights reserved
 *
 * Author: Timothy Olsen
 */
package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;

/**
 * @author timo
 *
 */
public class PARManager extends SecureScreen {

	/* (non-Javadoc)
	 * @see org.apache.turbine.modules.screens.VelocitySecureScreen#doBuildTemplate(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
	@Override
	protected void doBuildTemplate(RunData data, Context context) throws Exception {
		
	}

}
